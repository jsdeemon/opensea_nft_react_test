import nfts from '../data/NFTs.json'

export const fetchOwnCollection = async () => {
    const CONTRACT_ADDRESS = process.env.REACT_APP_CONTRACT_ADDRESS
  
    const options = {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'X-API-KEY': process.env.REACT_APP_OPENSEA_API as string,
      },
    };
  
    let collectionResponse: any;

    if (process.env.REACT_APP_MOCKED) {
         collectionResponse = nfts
    } else {
         collectionResponse = await fetch(
            `https://api.opensea.io/v2/orders/ethereum/seaport/listings?asset_contract_address=${CONTRACT_ADDRESS}&order_by=created_date&order_direction=desc`,
            options,
          ).then(response => response.json()).catch(err => {
              console.log(err)
          });
    }
  
    return collectionResponse;
  };