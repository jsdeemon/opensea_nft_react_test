import nftsJson from '../data/NFTs'

export const getNFTs = () => {
    const nfts = JSON.parse(nftsJson);
    return nfts
}