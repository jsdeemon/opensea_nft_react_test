import React, { useEffect, useState } from 'react';
import './App.css';
import Card from './components/Card';
import { fetchOwnCollection } from './utils/fetchOwnCollection';


const App: React.FunctionComponent = () => {

  const [collection, setCollection] = useState<any>({})

async function fetchNFTs() {
  const collectResponse = await fetchOwnCollection()
  setCollection(collectResponse)
}

  useEffect(() => {
fetchNFTs();
  }, [])

  return (
    <div className="container-fluid">
      <div className="row">
        
      { collection ? collection.owned?.map((nft: any) => (
        <>
        <Card 
        address={collection.contractAddress}
        key={Math.random()}
     item={nft}
        /> 
       
</>
      )) : <h1>There are no NFTs</h1> }
  
    </div>
    </div>
    
  );
}

export default App;
