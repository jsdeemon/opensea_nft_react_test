import React, { useState } from "react";
import Modal from "./Modal/Modal";


interface Props {
    item: any;
    address: any;
}

const Card: React.FunctionComponent<Props> = ({item, address}) => {

   const [modalActive, setModalActive] = useState(false);

    return (
      <>   
        <div className="col-md-4" >
<div className="card" >
  <div className="card-body">
    <h5 className="card-title">{item.name}</h5>
    <button 
   onClick={() => setModalActive(true)}
    className="btn btn-primary">Show NFT details</button>
  </div>
</div>
</div>

<Modal
active={modalActive}
setActive={setModalActive}
>
<div className="card text-center" >
  <div className="card-body">
  <img src={item.img} className="card-img-top" alt="..." />
    <h5 className="card-title">{item.name}</h5>
    <p className="card-text">Item id: {item.id}</p>
    <p>Contract address: {address}</p>
    <a
    href={`https://opensea.io/assets/ethereum/${address}/${item.id}`}
    className="btn btn-primary">Buy NFT</a>
  </div>
</div>
  </Modal>
</>
    );
}

export default Card;

