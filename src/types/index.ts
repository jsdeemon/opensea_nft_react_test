/**
 * Simple, unannotated non-fungible asset spec
 */

interface WyvernSchemaName{} 

export interface Asset {
    // The asset's token ID, or null if ERC-20
    tokenId: string | null,
    // The asset's contract address
    tokenAddress: string,
    // The Wyvern schema name (defaults to "ERC721") for this asset
    schemaName?: WyvernSchemaName,
    // Optional for ENS names
    name?: string,
    // Optional for fungible items
    decimals?: number
  } 

  export interface Iitem {
    description: string;
    external_url: string;
    image: string;
    name: string;
    attributes: any;
}